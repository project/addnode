$Id $
    Addnode
Addnode is a widget to be used with the nodereference CCK field type. It allows the user to either select items in a list (in the 
normal way) or create new items in a form on the same page. Note that add_n_reference is similar, but sends the user to the create-node 
page for that node type, rather than allowing them to create it on the same page.
